<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging()); //one middleware

$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();

    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            "body" => $msg->body,
            "user_id" => $msg->user_id,
            "created_at" => $msg->created_at,
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('user_id','');
    $_message = new Message();
    $_message->body = $message;
    $_message->user_id = $userid;
    $_message->save();

    
    if($_message->id){
        $payload = ['message_id' => $_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']);
    $message->delete();

    if($message->exists){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    $_message->body = $message;

    if($_message->save()){ //אם הערך נשמר
        $payload = ['message_id' => $_message->id,"result" => "The message has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();

    Message::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

$app->post('/auth', function($request, $response,$args){
    $user =  $request->getParsedBodyParam('user','');//מגיע מהפוסט
    $password =  $request->getParsedBodyParam('password','');//מגיע מהפוסט
    //we need to go to the DB, but not now
    if($user == 'Jack' && $password == '1234'){
      //we need to generate jwt but not now
        $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6IkphY2siLCJhZG1pbiI6dHJ1ZX0.lT4jR05dBKPUqdZdehGvTScnNJGrVCqCHNg-IkeGrAU'];
        return $response->withStatus(200)->withJson($payload);
    } else {
        $payload = ['token'=>null];
         return $response->withStatus(403)->withJson($payload);
    }
    
});
//בודק כל ג'ייסון האם הוא מאומת
$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => "supersecret",
    "path" =>['/messages']//כל הנתיבים שאנחנו רוצים שהמידלוואר יפקח עליהם
]));



$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

$app->run();